package server

import (
	"fmt"
	"github.com/bertof/blacklist/pkg/client"
)

type ClientAccount struct {
	Client *client.Client `json:"client"`
	Banned bool           `json:"banned"`
}

type Server struct {
	ClientMap       map[client.Id]*ClientAccount `json:"client_map"`
	clientReceiver  chan *client.Message
	workerReceivers []chan *client.Message
}

func New() *Server {
	return &Server{
		ClientMap:       map[client.Id]*ClientAccount{},
		workerReceivers: []chan *client.Message{},
		clientReceiver:  make(chan *client.Message),
	}
}

func (s *Server) Receiver() *chan *client.Message {
	return &s.clientReceiver
}

func (s *Server) Register(c *client.Client) {
	s.ClientMap[c.Id] = &ClientAccount{
		Client: c,
		Banned: false,
	}
}

func (s *Server) Ban(id client.Id) {
	if c, ok := s.ClientMap[id]; ok {
		c.Banned = true
	}
}

func (s *Server) UnBan(id client.Id) {
	if c, ok := s.ClientMap[id]; ok {
		c.Banned = false
	}
}

func (s *Server) Run(workers uint) {
	for i := uint(0); i < workers; i++ {
		s.runReceiver()
	}
	fmt.Println("Workers", len(s.workerReceivers))
	s.runBroker()
}

func (s *Server) runBroker() {
	go func() {
		counter := 0
		for m := range s.clientReceiver {
			counter += 1
			go func() {
				// Round robin on the workers
				s.workerReceivers[counter%len(s.workerReceivers)] <- m
			}()
		}
	}()
}

func (s *Server) runReceiver() {
	r := make(chan *client.Message)
	s.workerReceivers = append(s.workerReceivers, r)
	go func(r chan *client.Message) {
		for m := range r {
			//TODO test with goroutine
			fmt.Println("Worker received", m)
			s.handleMessage(m)
		}
	}(r)
}

func (s *Server) handleMessage(m *client.Message) {
	if c, ok := s.ClientMap[m.To]; ok && !c.Banned {
		// Good message
		go func() {
			*c.Client.Channel() <- m
			go func() {
				*m.ResponseChannel() <- &client.ResponseMessage{
					Content: "Message sent",
				}
			}()
		}()
	} else if !ok {
		// Recipient not found
		go func() {
			*m.ResponseChannel() <- &client.ResponseMessage{
				Content: "Recipient not found",
			}
		}()
	} else {
		// Recipient Banned
		go func() {
			*m.ResponseChannel() <- &client.ResponseMessage{
				Content: "Recipient Banned from the server",
			}
		}()
	}
}
