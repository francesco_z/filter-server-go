package client

import "fmt"

type Id = uint

type Client struct {
	Id              Id
	channel         *chan *Message
	responseChannel *chan *ResponseMessage
}

func New(id Id) *Client {
	m := make(chan *Message)
	r := make(chan *ResponseMessage)
	c := &Client{
		Id:              id,
		channel:         &m,
		responseChannel: &r,
	}
	c.Run()
	return c
}

func (c *Client) Channel() *chan *Message {
	return c.channel
}

func (c *Client) ResponseChannel() *chan *ResponseMessage {
	return c.responseChannel
}

func (c *Client) Run() {
	go func() {
		for m := range *c.channel {
			fmt.Println(c.Id, "received message:", m)
		}
	}()

	go func() {
		for m := range *c.responseChannel {
			fmt.Println(c.Id, "received response:", m)
		}
	}()
}
