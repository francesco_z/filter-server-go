package client

import "testing"

func TestMessage_String(t *testing.T) {
	m := Message{
		To:      1,
		Content: "Text",
	}
	if m.String() != "1 <- M[Text]" {
		t.Error("Invalid text")
	}
}

func TestResponseMessage_String(t *testing.T) {
	r := ResponseMessage{
		Content: "Text",
	}
	if r.String() != "R[Text]" {
		t.Error("Invalid text")
	}
}
